<?php
namespace ParserParcel;

use DiDom\Document;
use DiDom\Query;

class Parser
{
    const FORM_URL = 'https://vas.brt.it/vas/sped_det_show.hsm';

    const LANG = 'en';

    /**
     * @param $id
     * @return array
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public function getParcelData($id): array
    {
        $result = $this->getPageData($id);

        $document = new Document($result);
        $posts = $document->find('//*[@id="box_contenuti"]/table[2]/tbody/tr', Query::TYPE_XPATH);

        $results = [];

        foreach ($posts as $columns_index => $item){
            if ($columns_index === 0) {
                continue;
            }
            $items = $item->find('td');

            foreach ($items as $index => $item) {
                $key = '';

                switch ($index) {
                    case 0:
                        $key = 'Date';
                        break;
                    case 1:
                        $key = 'Time';
                        break;
                    case 2:
                        $key = 'Branch';
                        break;
                    case 3:
                        $key = 'Scan';
                        break;
                }
                $results[$columns_index][$key] = $item->text();
            }
        }

        sort($results);

        return $results;
    }

    /**
     * @param array $ids
     * @return array
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public function getMultipleParcelData(array $ids): array
    {
        $results = [];

        foreach ($ids as $id) {
            $results[$id] = $this->getParcelData($id);
        }

        return $results;
    }

    /**
     * @param array $ids
     * @return \Generator
     * @throws \DiDom\Exceptions\InvalidSelectorException
     */
    public function getMultipleParcelDataGenerator(array $ids): \Generator
    {
        foreach ($ids as $id) {
            yield $this->getParcelData($id);
        }
    }

    /**
     * @param $id
     * @return bool|string
     */
    protected function getPageData($id)
    {
        $handle = curl_init(self::FORM_URL);

        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, [
            'ChiSono' => $id
        ]);
        curl_setopt($handle, CURLOPT_HTTPHEADER, [
            "Cookie: lang=" . self::LANG,
        ]);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

        return curl_exec($handle);
    }
}
