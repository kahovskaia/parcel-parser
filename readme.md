# How to use
Initialize the class:
``` 
    $parser = new ParserParcel\Parser();
```

If you want to use one key to parse:

``` 
    $result = $parser->getParcelData('062151035463316');
```

Also you can get result with array of keys:

``` 
    $result = $parser->getMultipleParcelData([
        '062151035463316',
        '062151035463316'
    ]);
```

Or you can use generator to waste less memory:

``` 
    $result = $parser->getMultipleParcelDataGenerator([
        '062151035463316',
        '062151035463316'
    ]);
```
## Notes
* Please, be sure that you send id as string because some ids can start from `0`
and interpreter will recognize it as octal value not decimal and reformat it.
* You can specify language inside `Parser` class in `LANG` constant. Available: `en`, `it`. 
