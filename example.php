<?php
use ParserParcel\Parser;

require __DIR__.'/vendor/autoload.php';

$class = new Parser();

$result = $class->getParcelData('062151035463316');

$array_of_results = $class->getMultipleParcelData([
    '062151035463316'
]);

$generator = $class->getMultipleParcelDataGenerator([
    '062151035463316', '062151035463316'
]);
